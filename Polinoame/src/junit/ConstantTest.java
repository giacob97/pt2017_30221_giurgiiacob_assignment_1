package junit;

import static org.junit.Assert.*;
import org.junit.Test;
import code.*;
import java.util.*;
public class ConstantTest {

	@Test
	public void test() throws Exception {
		Scanner sc = new Scanner(System.in);
		String polinom = sc.nextLine();
		Polinom p = new Polinom(polinom);
		boolean hasConstant = p.hasConstant();
		if(hasConstant){
			assertTrue("Acest polinom contine constanta",true);
		}
		else
		{
			assertFalse("Polinomul introdus nu contine constanta",true);
		}
		sc.close();
	}

}
