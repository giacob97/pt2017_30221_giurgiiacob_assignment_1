package junit;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import code.Operations;
import code.Polinom;

public class DivisionTest {

	@Test
	public void test() throws Exception {
		Scanner sc = new Scanner(System.in);
		String polinom = sc.nextLine();
		Polinom p = new Polinom(polinom);
		String secPolinom = sc.nextLine();
		Polinom q = new Polinom(secPolinom);
		Operations o = new Operations();
		o.Impartire(p, q);
		System.out.println(p.getMonoms());
		if(p.getMonoms().equals("0")){
			assertTrue("Polinomul q e divizibil cu p",true);
		}
		else
		{
			assertTrue("Polinoamele nu sunt dizivibile",false);
		}
		sc.close();
	}

}
