package junit;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import code.Operations;
import code.Polinom;

public class DerivateTest {

	@Test
	public void test()throws Exception {
		Scanner sc = new Scanner(System.in);
		String polinom = sc.nextLine();
		Polinom p = new Polinom(polinom);
		Operations o = new Operations();
		Polinom aux = o.Derivare(p); //daca se da de la tine x^2 +2x
		assertEquals("2x+2",aux.getMonoms());
		sc.close();
	}
}
