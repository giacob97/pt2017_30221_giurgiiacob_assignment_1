package junit;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import code.Operations;
import code.Polinom;

public class MultiplyTest {

	@Test
	public void test() throws Exception {
		Scanner sc = new Scanner(System.in);
		String polinom = sc.nextLine();
		Polinom p = new Polinom(polinom);
		String secPolinom = sc.nextLine();
		Polinom q = new Polinom(secPolinom);
		Operations o = new Operations();
		Polinom aux = o.Inmultire(p, q);
		assertEquals("x^2+3x+2",aux.getMonoms());
		sc.close();
	}

}
