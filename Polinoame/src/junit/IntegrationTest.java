package junit;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.Test;

import code.Operations;
import code.Polinom;

public class IntegrationTest {

	@Test
	public void test() throws Exception {
		Scanner sc = new Scanner(System.in);
		String polinom = sc.nextLine();
		Polinom p = new Polinom(polinom);
		Operations o = new Operations();
		Polinom aux = o.Integration(p); //daca se da de la tine 3x^2 +2
		assertEquals("x^3+2x",aux.getMonoms());
		sc.close();
	}

}
