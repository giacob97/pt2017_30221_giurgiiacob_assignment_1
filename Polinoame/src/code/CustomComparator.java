package code;

import java.util.Comparator;

public class CustomComparator implements Comparator<Monom> {

	@Override
	public int compare(Monom o, Monom p) {
		// TODO Auto-generated method stub
		return new Integer(p.getDegree()).compareTo(o.getDegree());
	}

}
