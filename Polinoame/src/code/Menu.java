package code;


import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class Menu extends JFrame{
	
	private JPanel contentPane;
	private final JLabel lbBackGround = new JLabel();
	Operations o = new Operations();
	
	
	public Menu(){
		setTitle("Meniu operatii\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JButton add = new JButton("Add\r\n");
		JButton sub = new JButton("Sub\r\n");
		JButton div = new JButton("Div\r\n");
		JButton mul = new JButton("Mul\r\n");
		JButton der = new JButton("Der\r\n");
		JButton integration = new JButton("Int\r\n");
		JLabel p = new JLabel("Polinom P\r\n"); 
		JLabel q = new JLabel("Polinom Q\r\n"); 
		JLabel r = new JLabel("Result\r\n"); 
		JTextField textField = new JTextField(10);
		JTextField textField_q = new JTextField(10);
		JTextField result = new JTextField(10);
		JLabel re = new JLabel("Rest\r\n"); 
		re.setBounds(56, 320, 100, 50);
		 
		result.setBounds(175, 290, 200, 25);
		textField.setBounds(175, 68, 200, 20);
		textField_q.setBounds(175,118,200,20);
		r.setBounds(56, 278, 100, 50);
	    p.setBounds(56, 51, 100, 50);
	    q.setBounds(56, 101, 100, 50);
		add.setBounds(50 , 220 ,65,30);
		sub.setBounds(130 ,220,65,30);
		mul.setBounds(210, 220, 65, 30);
		div.setBounds(290, 220, 65, 30);
		der.setBounds(370, 220, 65, 30);
		integration.setBounds(450,220,65,30);
		JTextField rest = new JTextField(10);
		 rest.setBounds(175, 335, 200, 20);
		 contentPane.add(rest);
		contentPane.add(re);
		contentPane.add(q);
		contentPane.add(p);
		contentPane.add(r);
		contentPane.add(result);
		contentPane.add(textField);
		contentPane.add(textField_q);
		contentPane.add(integration);
		contentPane.add(der);
		contentPane.add(div);
		contentPane.add(mul);
		contentPane.add(sub);	
		contentPane.add(add);
		//Buton de suma
		
		
		add.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				try{   String strp= textField.getText() ;
				String strq= textField_q.getText();
				Polinom p = new Polinom(strp);
				Polinom q = new Polinom(strq);
				Polinom add = o.Adunare(q,p);
				result.setText(add.getMonoms());
				JOptionPane.showMessageDialog(null,"The sum has been done");
				}catch(Exception f){
					JOptionPane.showMessageDialog(null,"Ati introdus gresit polinomul dorit");
					result.setText("0");
				}
			}
		});
		//Buton de diferenta
		sub.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				try{
					String strp= textField.getText() ;
					String strq= textField_q.getText();
					Polinom p = new Polinom(strp);
					Polinom q = new Polinom(strq);
					Polinom sub = o.Scadere(q, p);
					   result.setText(sub.getMonoms());
					JOptionPane.showMessageDialog(null,"The subtraction has been done");
				}catch(Exception f){
					JOptionPane.showMessageDialog(null,"Ati introdus gresit polinomul dorit");
				}
			}	
		});
	   //Buton de inmultire
		mul.addActionListener(new ActionListener(){	
			public void actionPerformed(ActionEvent arg0){
				try{
					String strp= textField.getText() ;
					String strq= textField_q.getText();
					Polinom p = new Polinom(strp);
					Polinom q = new Polinom(strq);
					Polinom s = o.Inmultire(q, p);
					result.setText(s.getMonoms());
					JOptionPane.showMessageDialog(null,"The multiply has been done");
				}catch(Exception f){//JOptionPane.showMessageDialog(null,"Ati introdus gresit polinomul dorit");
					result.setText("0"); // cand inmultim cu 0 , va prinde exceptia si rezultatul va fi null;
				}
			}
		});
	    //Buton de impartire
		div.addActionListener(new ActionListener(){	
		     public void actionPerformed(ActionEvent arg0){
		    	 try{
		    		 
		    		 String strp= textField.getText() ;
		    		 String strq= textField_q.getText();
					 Polinom p = new Polinom(strp);
					 Polinom q = new Polinom(strq);
					 Polinom s = o.Impartire(p,q);
					 result.setText(s.getMonoms());
					 rest.setText(o.rest);
					 JOptionPane.showMessageDialog(null,"The division has been done");
					 }catch(Exception f){
						JOptionPane.showMessageDialog(null,f.getMessage());
					 }	
		     }			
		});		
		//Buton de derivare
		der.addActionListener(new ActionListener(){		
		   public void actionPerformed(ActionEvent arg0){
			   try{
				    String strp= textField.getText() ;
					Polinom p = new Polinom(strp);
					o.Derivare(p);
					result.setText(p.getMonoms());
					JOptionPane.showMessageDialog(null,"The derivation has been done");
					}catch(Exception f){
						JOptionPane.showMessageDialog(null,"The derivation is available for polinom P!");
					}					
		   }			
		});
		//Buton de integrare
		integration.addActionListener(new ActionListener(){		
			public void actionPerformed(ActionEvent arg0){
				  try{String strp= textField.getText() ;
						Polinom p = new Polinom(strp);
						Polinom q = o.Integration(p);
						result.setText(o.getIntegratedMonoms(q));
						JOptionPane.showMessageDialog(null,"The integration has been done");
						}catch(Exception f){
							JOptionPane.showMessageDialog(null,"The integration is available for polinom P");
						}				
			}
		});
	    lbBackGround.setBounds(0, -95, 614, 498);
		Image img1 = new ImageIcon(this.getClass().getResource("/TP_2.PNG")).getImage();
		lbBackGround.setIcon(new ImageIcon(img1));
		contentPane.add(lbBackGround);	
    }
}
