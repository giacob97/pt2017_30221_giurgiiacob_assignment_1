package code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;



public class Polinom{

	public ArrayList<Monom> monoms = new ArrayList<>();
	
	
	public Polinom(){
		
	}
	
	public Polinom(String polinom) throws Exception{
		String[] parts = polinom.split(Pattern.quote(" "));
		setMonoms(parts);
	}
	
	
	public void setMonoms(String parts[]) throws Exception{
		for(int i = 0;i<parts.length;i++){
			 if(parts[i].indexOf("x") < 0){// pentru constante
				 Monom m = new Monom(Integer.parseInt(parts[i]),0);
				 monoms.add(m);
			 }
			 else if(parts[i].indexOf("^") < 0){ // pentru puterea 1
				 if(parts[i].charAt(parts[i].length()-1) != 'x'){ // daca nu se lasa spatiu dupa putere care aici e implicit 1
					 throw new Exception("Ati introdus gresit polinomul");
				 }
				 try{
				 String coeff[] = parts[i].split("x");
				 Monom m = new Monom(Integer.parseInt(coeff[0]),1);
				 monoms.add(m);
				 }catch(Exception e){
					 if(parts[i].indexOf("-") > 0){
						 Monom m = new Monom(-1,1);
					     monoms.add(m);
					 }
					 else{
					 Monom m = new Monom(1,1);
				     monoms.add(m);
					 }
				 }
			 }
			 else{
			 String coeff[] = parts[i].split("x"); // luam coeficientul inainte de x
	 		 String grade[] = parts[i].split(Pattern.quote("^")); //luam gradul care se gaseste dupa ^
		     if(grade[1].indexOf('+') > 0 || grade[1].indexOf('-')>0){ //trebuie lasat spatiu dupa puterea monomului
		    	 throw new NumberFormatException("Ati introdus gresit polinomul");
		     }
		     else{
	 		 try{
	 		 Monom m = new Monom(Integer.parseInt(coeff[0]),Integer.parseInt(grade[1]));
		     monoms.add(m);
		     }catch(Exception e){
		    	 if(coeff[0].indexOf("-") < 0){
		    	 Monom m = new Monom(1,Integer.parseInt(grade[1]));
			     monoms.add(m);
		    	 }
		    	 else {
		    		 Monom m = new Monom(-1,Integer.parseInt(grade[1]));
				     monoms.add(m);
		    	 }
		     }
		     }
			 }
		}		
	}
	
	
	public void sortare(){
		   Collections.sort(monoms,new CustomComparator());
	}
	
	
	public void eliminateNullCoeff(){
		for(int i = 0 ; i< monoms.size() ;i++){
			if(monoms.get(i).getCoeff() == 0){
				monoms.remove(i);
			}
		}
	}
	
	public Polinom Reduce(Polinom p){
		p.eliminateNullCoeff();
		p.sortare();
		for(int i = 0 ; i<p.monoms.size()-1;i++){
			if(p.monoms.get(i).getDegree() == p.monoms.get(i+1).getDegree()){
				p.monoms.get(i).setCoeff( p.monoms.get(i).getCoeff()+p.monoms.get(i+1).getCoeff());
				p.monoms.remove(p.monoms.get(i+1));
				i--;
			}
		}
		return p;
	}
	
	
	public boolean hasConstant(){
		for(Monom m : monoms){
			if(m.getDegree() == 0){
				return true;
			}
		}
		return false;
	}
	
	public String getMonoms(){
		
		String result = "";
		sortare();
		Monom first = monoms.get(0);// cea mai mare putere din polinom
		for(Monom m : monoms){
			 if(m.equals(first) && m.getCoeff()<0){
				 result += "";
			 }
			 else if(m.getCoeff() > 0 && !m.equals(first)){
				 result += "+";
			 }
			 else if(m.getCoeff() < 0){
				 result += "";
			 }
			
			 if (m.getDegree() == 0 && m.getCoeff()==0){
				 result += "0";
			 }
			 else if(m.getCoeff() == 0){
				 result +="";
			 }
			 else if (m.getDegree() == 0){
				 result += (int)m.getCoeff();
			 }
			 else if(m.getDegree() == 1 && m.getCoeff() == 1){
				 result += "x";
			 }
			 else if(m.getCoeff() == 1){
				 result += "x^" + (int)m.getDegree();
			 }
			 else if(m.getDegree() == 1){
				 result +=(int)m.getCoeff() +  "x"; 			 }
			 else {
				 result += (int)m.getCoeff() + "x^" + m.getDegree() + " ";
			 }
		}
		if(result.equals(""))
			result += "0"; // in acest caz polinomul rezultat este 0; ex scadere 4x-4x;
		return result;
	}
}
 
