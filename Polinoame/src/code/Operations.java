package code;


public class Operations{
    String rest;
	public Operations(){
	}
	public boolean coefNull(Polinom p){
		
		for(Monom nul : p.monoms){
			if(nul.getDegree() != 0 || nul.getCoeff()!=0){
				return false;
			}
		}
		return true;
	}
	
	public Polinom Adunare(Polinom p , Polinom r){
		Polinom add = new Polinom();
		for(Monom m : r.monoms)
			add.monoms.add(m);
		for(Monom m : p.monoms)
			add.monoms.add(new Monom(m.getCoeff(),m.getDegree()));
		add.Reduce(add);
		return add;
	}
	
	
	public Polinom Scadere(Polinom p , Polinom r){
		Polinom sub = new Polinom();
		for(Monom m : r.monoms)
			sub.monoms.add(m);
		for(Monom m : p.monoms)
			sub.monoms.add(new Monom(-m.getCoeff(),m.getDegree()));
		sub.Reduce(sub);
		return sub;
	}
	
	
	public Polinom Inmultire(Polinom p , Polinom r){
		Polinom s = new Polinom();
		for(Monom m : p.monoms){
			for(Monom m1 : r.monoms){
			  Monom aux = new Monom((m.getCoeff() * m1.getCoeff()),(m.getDegree() + m1.getDegree()));
			  s.monoms.add(aux);
			}
		}
		s.Reduce(s);
		s.getMonoms();
		return s;
	}
	// incercare cu try and catch pt cazul in care polinoamele sunt divizibile
	public Polinom Impartire(Polinom p , Polinom r){
		Polinom aux = new Polinom();
		
		p.sortare();
		r.sortare();
	   if(coefNull(r)){
		   throw new ArithmeticException("You cannot divide by 0!");
	   }
	   
	   else{ 
	   
	       try{
	       while(p.monoms.get(0).getDegree() >= r.monoms.get(0).getDegree()){	      
		   Monom cn = new Monom((p.monoms.get(0).getCoeff()/r.monoms.get(0).getCoeff()),(p.monoms.get(0).getDegree() - r.monoms.get(0).getDegree()));
		   aux.monoms.add(cn);
		   Polinom aux2 = new Polinom();
		   aux2.monoms.add(cn);
		   Polinom pr = Inmultire(aux2,r);
		   p=Scadere(pr,p);
		   p.Reduce(p);
		   //System.out.println(p.getMonoms());
		  
	       }
	       
	       }catch(Exception e){
	    	   System.out.println();
		       aux.getMonoms();
	       }  
	   //System.out.println(aux.getMonoms());
	   rest = p.getMonoms();
	   return aux;
	   }
	}
	
	
	
	public Polinom Derivare(Polinom p){
		Polinom der = new Polinom();
		for(Monom m: p.monoms){
			m.setCoeff(m.getCoeff()*m.getDegree());
			m.setDegree(m.getDegree()-1);
			if(m.getDegree() >=0){
				der.monoms.add(m);
			}
			else{
				der.monoms.add(new Monom(m.getCoeff(),0));
			}
		}	
		der.getMonoms();
		return der;	
    }
	
	public Polinom Integration(Polinom p){
		Polinom integrated = new Polinom();
		for(Monom m: p.monoms){
			Monom q = new Monom((float)m.getCoeff()/(m.getDegree()+1) ,m.getDegree()+ 1 );
			integrated.monoms.add(q);
		}
		return integrated;
	}
	
	
	public String getIntegratedMonoms(Polinom p){
		String result="";
		for(Monom m : p.monoms){
			if(Math.floor(m.getCoeff())== m.getCoeff())
			result += (int)m.getCoeff() + "x^" + m.getDegree() + " ";
			else
				result += m.getCoeff() + "x^" + m.getDegree() + " ";
		}
		return result;
	}
}	
